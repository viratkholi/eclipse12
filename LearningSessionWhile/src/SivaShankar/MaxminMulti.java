package SivaShankar;

public class MaxminMulti {

    public static void main(String[] args) {
        int[][] mark = {
            {10, 20, 30, 40, 1},
            {11, 12, 13, 14, 50},
            {1,2,3,4,5}
        };
        
        int overallTotal = 0;  // Variable to store the overall total sum of all elements

        for (int i = 0; i < mark.length; i++) {  // Loop through each row
            int rowTotal = 0;
            int min = mark[i][0];  // Initialize min with the first element of the current row
            int max = mark[i][0];  // Initialize max with the first element of the current row
            
            // Loop through each element in the row
            for (int j = 0; j < mark[i].length; j++) {
                System.out.println(mark[i][j]);
                rowTotal += mark[i][j];  // Add the current element to the row total
                overallTotal += mark[i][j];  // Add the current element to the overall total
                
                if (mark[i][j] < min) {
                    min = mark[i][j];  // Update min if the current element is smaller
                }
                
                if (mark[i][j] > max) {
                    max = mark[i][j];  // Update max if the current element is larger
                }
            }
            
            int avg = rowTotal / mark[i].length;  // Calculate the average for the current row
            
            // Print the total sum, average, minimum, and maximum of the current row
            System.out.println("Total sum of row " + (i + 1) + ": " + rowTotal);
            System.out.println("Average of row " + (i + 1) + ": " + avg);
            System.out.println("Minimum of row " + (i + 1) + ": " + min);
            System.out.println("Maximum of row " + (i + 1) + ": " + max);
        }
        
        // Print the overall total sum of the array
        System.out.println("Overall total sum of the array: " + overallTotal);
    }
}

