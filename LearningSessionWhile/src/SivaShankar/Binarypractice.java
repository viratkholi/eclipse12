package SivaShankar;

import java.util.Arrays;

public class Binarypractice {

    // Binary search function
    static int binarySearch(int[] arr, int target) {
        int left = 0;
        int right = arr.length - 1;

        while (left <= right) {
            int mid = left + (right - left) / 2;

            // Check if target is present at mid
            if (arr[mid] == target)
                return mid;

            // If target greater, ignore left half
            if (arr[mid] < target)
                left = mid + 1;

            // If target is smaller, ignore right half
            else
                right = mid - 1;
        }

        // If target is not found
        return -1;
    }

    public static void main(String[] args) {
        int[] arr = {2, 4, 3, 10, 40, 5};
        
        // Sort the array
        Arrays.sort(arr);
        
        // Print the sorted array (for debugging purposes)
        System.out.println("Sorted array: " + Arrays.toString(arr));
        
        int target = 2;
        int result = binarySearch(arr, target);
        if (result == -1)
            System.out.println("Element not present");
        else
            System.out.println("Element found at index " + result);
    }
}
