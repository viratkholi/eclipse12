package ExceptionHandling;

import java.nio.InvalidMarkException;
import java.util.Iterator;

public class HighAttendanceException {

	public static void main(String[] args) {
		
		try {
			checkattendance(57);
		}
		catch(Exception se){
			se.printStackTrace();
		}
		display();
	}

	private static void display() {
		System.out.println("Display");
	}

	private static void checkattendance(int i) throws LowAttendanceException {
		if(i>56) {
			System.out.println("good");
		}
		else {
			LowAttendanceException gg = new LowAttendanceException();
			throw gg;
		}
		
	}

}
