package ExceptionHandling;

public class LowAttendanceException extends Exception{ 
	 
	 public static void main(String[] args) { 
	   
	  try { 
	   checkAttendance(75); 
	  } catch (LowAttendanceException e) { 
	   e.printStackTrace(); 
	  } 
	  add(); 
	 
	 } 
	 
	 private static void add() { 
	  System.out.println("add method"); 
	   
	 } 
	 
	 private static void checkAttendance(int percentage) throws LowAttendanceException { 
	  if(percentage>=75) { 
	   System.out.println("Allow exam"); 
	  }else { 
	   LowAttendanceException lae = new LowAttendanceException(); 
	   throw lae; 
	  } 
	   
	 } 
	 
	}

