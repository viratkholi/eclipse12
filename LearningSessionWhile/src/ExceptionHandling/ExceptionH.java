package ExceptionHandling;

import java.util.Scanner;

public class ExceptionH {

	public static void main(String[] args) {
		ExceptionH eh = new ExceptionH();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter No1");
		int No1 = sc.nextInt();
		System.out.println("Enter No2");
		int No2 = sc.nextInt();
		
		
		eh.add(No1,No2);
		eh.div(No1,No2);
		eh.sub(No1,No2);
		
		ExceptionH eh1 = new ExceptionH();
		eh.display(eh1);      //method la object pass pandrom
		
	}
	public void display(ExceptionH eh2)  //class vachi object pass pandrom (eh1=eh2) 
	{                                   // eh2 = eh1 object oda class 
		
	}

	private void add(int no1, int no2) {
		
		int no = no1 + no2;
		System.out.println("add " + no);
	}

	private void div(int no1, int no2) {
		
		int no = no1 / no2;
		System.out.println("div " + no);
		
	}

	private void sub(int no1, int no2) {
		
		int no = no1 - no2;
		System.out.println("sub " + no);
	}

}
