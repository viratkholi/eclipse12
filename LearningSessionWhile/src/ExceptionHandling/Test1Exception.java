package ExceptionHandling;

//unchecked exception

public class Test1Exception extends RuntimeException {

	public static void main(String[] args) {
	     
		castvote(16);
		count();
	}

	private static void castvote(int age) {
		if(age >= 18) {
			System.out.println("Allow");
		}
		else {
			Test1Exception gg = new Test1Exception();
			throw gg;
			//System.out.println("Dont allow");
		}
		
	}
    private static void count() {
		int total = 0;
		total = total + 1;
		System.out.println(total);
	}

}
