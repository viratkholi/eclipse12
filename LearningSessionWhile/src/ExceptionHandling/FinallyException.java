package ExceptionHandling;

import java.io.File; 
import java.io.IOException; 
	 
	public class FinallyException { 
	 
	 public static void main(String[] args) throws IOException { 
	   
	  finallyDemo(); 
	  throwsDemo(); 
	   
	 } 
	 
	  
	 private static void throwsDemo() throws IOException{ 
	  File file = new File("/home/thaniga/Documents/Notepad"); 
	  file.createNewFile(); 
	 } 
	 
	 private static void finallyDemo() { 
	  try { 
	     int[] myNumbers = {1, 2, 3}; 
	    System.out.println(myNumbers[10]); 
	   } catch (Exception e) { 
	     System.out.println("Something went wrong."); 
	   } finally { 
	     System.out.println("The 'try catch' is finished."); 
	   } 
	   
	 } 
	 
	}


