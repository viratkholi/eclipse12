package Objects;

public class Wrapperun {

	public static void main(String[] args) {
		
		Integer nn = new Integer(21);
		Wrapperun kk = new Wrapperun();
		kk.add(nn);

	}

//	private void add(Integer nn) {         //auto boxing = primitive type to wrapper
//		System.out.println("Quick");
//	}
	private void add(int nn) {
		System.out.println("Slow");        //auto unboxing = wrpper type to primitive
	}

}
