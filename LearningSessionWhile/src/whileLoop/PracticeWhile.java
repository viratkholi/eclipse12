package whileLoop;

public class PracticeWhile {

	public static void main(String[] args) {
	 
		PracticeWhile lw = new PracticeWhile();
//		lw.display(0,4);
//		System.out.println();
//		lw.display(1,5);
//		System.out.println();
//		lw.display(10,15);
//dec		lw.display(10,2,2);
//inc		lw.display(10,2,2);
	
//	System.out.println();
		int st=1;
		while(st <= 10) {
			lw.display(st,10,1);
			System.out.println();
			st+=1;
		}

	}

	private void display(int st, int j, int k) {
		while(st<=j) {
			System.out.print(st+" ");
			st+=k;
		}
		
	}

//	private void display(int no, int condition, int dec) {
//		while(no >= condition) {
//			System.out.println(no+" ");
//			no-=dec;
//		}
//	     
//		
//	}

//	private void display(int no, int condition, int increment) {
//		while(no<condition) {
//			System.out.print(no+" ");
//			no+=increment;
//		}
//		
//	}

//	private void display(int no, int condition) {
//		//int no =1;
//		while(no <= condition)
//		{
//			System.out.print(no+" ");
//			no++;
//		}
//		
//	}

}
