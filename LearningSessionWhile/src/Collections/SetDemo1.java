package Collections;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeSet;

public class SetDemo1 {

	public static void main(String[] args) {
        
		HashSet hss = new HashSet();
		hss.add("Thaniga");
		hss.add("Chennai");
		hss.add("Kamesh");
		hss.add("Erode");
		hss.remove("kamesh");
		
		System.out.println(hss);
		
		LinkedHashSet hss1 = new LinkedHashSet();
		hss1.add("Thaniga");
		hss1.add("Chennai");
		hss1.add("Kamesh");
		hss1.add("Erode");
		hss1.add("Erode");
		
		System.out.println(hss1);
		
		
		TreeSet hss2 = new TreeSet();
//		hss2.add("Thaniga");
//		hss2.add("Chennai");
//		hss2.add("Kamesh");
//		hss2.add("Erode");
//		hss2.add("Erode");
		hss2.add(111);	
		hss2.add(22);
		hss2.add(35);
		hss2.add(14);
		hss2.add(5);
		hss2.add(5);
		System.out.println(hss2);
	}

}
