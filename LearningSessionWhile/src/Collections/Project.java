package Collections;

import java.util.ArrayList;
import java.util.Scanner;

public class Project {
		private ArrayList<String> cart = new ArrayList<String>();//String cart;(object Creation)

//	    public Project() {
//	       
//	       cart = new ArrayList<>();
//	    }

	    public void addItem(String item) {
	        cart.add(item);
	        System.out.println(item + " added to the cart.");
	    }

	    public void removeItem(String item) {
	        if (cart.contains(item)) {
	            cart.remove(item);
	            System.out.println(item + " removed from the cart.");
	        } else {
	            System.out.println(item + " not found in the cart.");
	        }
	    }

	    public void displayCart() {
	        if (cart.isEmpty()) {
	            System.out.println("The cart is empty.");
	        } else {
	            System.out.println("Items in the cart: " + cart);
	        }
	    }

	    public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        Project shoppingCart = new Project();

	        while (true) {
	            System.out.println("Do you want to add or remove an item? (1 to add, 2 to remove, 3 to display cart, 0 to exit): ");
	            int choice = scanner.nextInt();
	            scanner.nextLine();  // Consume the newline

	            switch (choice) {
	                case 1:
	                    System.out.println("Enter the item to add: ");
	                    String addItem = scanner.nextLine();
	                    shoppingCart.addItem(addItem);
	                    break;
	                case 2:
	                    System.out.println("Enter the item to remove: ");
	                    String removeItem = scanner.nextLine();
	                    shoppingCart.removeItem(removeItem);
	                    break;
	                case 3:
	                    shoppingCart.displayCart();
	                    break;
	                case 0:
	                    System.out.println("Exiting...");
	                    scanner.close();
	                    return;
	                default:
	                    System.out.println("Invalid choice. Please try again.");
	            }
	        }
	}

}
