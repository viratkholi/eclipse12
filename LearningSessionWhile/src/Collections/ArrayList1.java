package Collections;

import java.util.ArrayList;

public class ArrayList1 {

	public static void main(String[] args) {
		  ArrayList1 all = new ArrayList1();
		  ArrayList all1 = new ArrayList();
		  all1.add(10);            
		  all1.add(10.5);         
		  all1.add(true);        
		  all1.add("Virat");
		  all1.add('K');
		  
		  
		  System.out.println(all1);
		  all1.add("Thaniga");
		  System.out.println(all1);
		  all1.add("Kalanidhi");
		  System.out.println(all1);
		  
		  //addall(collection)
		  ArrayList al2 = new ArrayList();
		  al2.addAll(all1);
		  System.out.println(al2);
		  
		  //addall(index, collection)
		  ArrayList al3 = new ArrayList();
		  al3.add("thaniga");
		  al3.add("Virat");
		  al3.add('K');
		  al3.add("Virat");
		  System.out.println(al3);
		  al3.addAll(3, al2);
		  System.out.println(al3);
		  
		  //contains and contains all
		  System.out.println(al3.contains(10.4));
		  System.out.println(al3.containsAll(all1));
		  
		  //get indexof and lastindexof
		  System.out.println(al3.get(1));
          System.out.println(al3.indexOf("Thaniga"));   //index - 0
          System.out.println(al3.lastIndexOf("Virat"));
          
          //isempty and remove
          System.out.println(al3.isEmpty());
          System.out.println(al3.remove(8));
          System.out.println("al3 contains ____" + al3);
          
          System.out.println("al2 contains ____" + al2);
//          al3.removeAll(al2);
//          System.out.println(al3);
          
          al3.retainAll(al2);
          System.out.println(al3);
          System.out.println(al3.size());
          System.out.println(al3.subList(1, 5));
	}

}

