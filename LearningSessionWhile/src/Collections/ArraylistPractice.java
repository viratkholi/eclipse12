package Collections;

import java.util.ArrayList;

public class ArraylistPractice {

	public static void main(String[] args) {
		
		ArrayList al = new ArrayList();
		ArraylistPractice al1 = new ArraylistPractice();
        al.add("Appa");
        al.add("Amma");
        al.add('S');
        al.add(18);
        System.out.println(al);
        
        ArrayList all = new ArrayList();
        all.addAll(al);
        all.add("Subashni");
        System.out.println(all);
        
        System.out.println(al.contains(18));
        System.out.println(al.containsAll(all));
        
        System.out.println(all.indexOf("Amma"));
        System.out.println(all.get(3));
        System.out.println(all.lastIndexOf('S'));
        
//        System.out.println(all.isEmpty());
//        System.out.println(all.removeAll(al));
        
         all.retainAll(al);
         System.out.println(all);   //Subashni has not come because we are reataining
        
         System.out.println(all.size());
         System.out.println(all.subList(1, 3));
        
	}

}
