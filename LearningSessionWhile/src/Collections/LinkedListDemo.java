package Collections;

import java.util.LinkedList;

public class LinkedListDemo {

	public static void main(String[] args) {
		//LINKED LIST
		LinkedListDemo all = new LinkedListDemo();
		  LinkedList al = new LinkedList();
		  al.add(10);            
		  al.add(10.5);         
		  al.add(true);        
		  al.add("Virat");
		  al.add('K');
		  
		  
		  System.out.println(al);
		  al.add("Thaniga");
		  System.out.println(al);
		  al.add("Kalanidhi");
		  System.out.println(al);
		  
		  //addall(collection)
		  LinkedList al2 = new LinkedList();
		  al2.addAll(al);
		  System.out.println(al2);
		  
		  //addall(index, collection)
		  LinkedList al3 = new LinkedList();
		  al3.add("thaniga");
		  al3.add("Virat");
		  al3.add('K');
		  al3.add("Virat");
		  System.out.println(al3);
		  al3.addAll(3, al2);
		  System.out.println(al3);
		  
		  //contains and contains all
		  System.out.println(al3.contains(10.4));
		  System.out.println(al3.containsAll(al));
		  
		  //get indexof and lastindexof
		  System.out.println(al3.get(1));
        System.out.println(al3.indexOf("Thaniga"));   //index - 0
        System.out.println(al3.lastIndexOf("Virat"));
        
        //isempty and remove
        System.out.println(al3.isEmpty());
        System.out.println(al3.remove(8));
        System.out.println("al3 contains ____" + al3);
        
        System.out.println("al2 contains ____" + al2);
//        al3.removeAll(al2);
//        System.out.println(al3);
        
        al3.retainAll(al2);
        System.out.println(al3);
        System.out.println(al3.size());
        System.out.println(al3.subList(1, 5));
	}

}
