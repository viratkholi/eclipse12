package Collections;

import java.util.HashMap;
import java.util.LinkedHashMap;

public class LinkedHashmap {

	public static void main(String[] args) {
		LinkedHashMap hm = new LinkedHashMap();
		hm.put("Virat", 18);
		hm.put("Dhoni", 07);
		hm.put("Raina", 03);
		hm.put("ABD", 17);
		hm.put("Rohith", 45);
		//hm.put("Ganguly", null);
		hm.put(null, 00);
		hm.put(20, null);
		System.out.println(hm);
		
		hm.put("Virat", 19);
		//hm.put("Ganguly", 9);
		hm.put('c', 90);
		System.out.println(hm);
		System.out.println();
		
		System.out.println(hm.containsKey("Virat"));
		System.out.println(hm.containsValue(07));
		System.out.println(hm.get("ABD"));
		System.out.println();
		
		System.out.println(hm.isEmpty());
		System.out.println(hm.putIfAbsent("Ganguly", 4569));
		System.out.println(hm.remove('c'));
		System.out.println(hm.size());
		System.out.println(hm);
		System.out.println();
		
		System.out.println(hm.entrySet());
		System.out.println(hm.keySet());
		System.out.println(hm.values());
		System.out.println();

	}

}
