package Collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

public class SetDemo {

	public static void main(String[] args) {
		//SET = NO Insertion and No Duplicates
		 HashSet hs = new HashSet();
		 
		 hs.add("THANIGA");
		 hs.add("RAMU");
		 hs.add("VIRAT");
		 hs.add("SIVA");
		 hs.add("KING");
		 hs.add("THANIGA");
		 System.out.println(hs);
		
		 ArrayList aa = new ArrayList();
		 aa.add(1);
		 aa.add(14);
		 aa.add(18);
		 aa.add(2001);
		 aa.add(1);
		 aa.add(14);
		 System.out.println(aa);
		 HashSet hs1 = new HashSet(aa);
		 System.out.println(hs1);
		 
		 //1. Arrays.aslist
		 Integer[] ir = {14,13,1,21,15,16,15,14};
		 List lo = Arrays.asList(ir);
		 System.out.println(lo);
		 HashSet hs2 = new HashSet(lo);
		 System.out.println(hs2);
		 
		 for (Object obje : hs2) {
			 if(obje.equals(14))
			 {
				 System.out.println(obje);
			 }
		}
		 
		 //2. Collections.addall()
		 String [] str = {"Chithra", "Thaniga", "Siva", "Subha", "Chithra"};
		 ArrayList al3 = new ArrayList();
		 Collections.addAll(al3, str);
		 System.out.println(al3);
		 
		 for (Object obj : al3) 
		 {
			 if(obj.equals("Chithra"))
			 {
			System.out.println(obj);
		     }
		 }
		 
		 //1 and 2 methods are same
		 

	}

}
