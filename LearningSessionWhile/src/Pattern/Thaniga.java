package Pattern;

public class Thaniga {

	public static void main(String[] args) {
		        // Loop for each row
		        for (int i = 0; i < 5; i++) {
		            // Print T
		            printT(i);

		            // Print H
		            printH(i);

		            // Print A
		            printA(i);

		            // Print N
		            printN(i);

		            // Print I
		            printI(i);

		            // Print G
		            printG(i);

		            // Print A
		            printA(i);

		            // Move to the next line for the next row
		            System.out.println();
		        }
		    }

		    // Print T
		    public static void printT(int row) {
		        for (int j = 0; j < 5; j++) {
		            if (j == 0 || row == 2) {
		                System.out.print("*");
		            } else {
		                System.out.print(" ");
		            }
		        }
		        System.out.print("  ");
		    }

		    // Print H
		    public static void printH(int row) {
		        for (int j = 0; j < 5; j++) {
		            if (j == 0 || j == 4 || row == 2) {
		                System.out.print("*");
		            } else {
		                System.out.print(" ");
		            }
		        }
		        System.out.print("  ");
		    }
		    // Print A
		    public static void printA(int row) {
		        for (int j = 0; j < 5; j++) {
		            if ((row == 2 && j != 0 && j != 4) || (j == 0 && row != 2) || (j == 4 && row != 2)) {
		                System.out.print("*");
		            } else {
		                System.out.print(" ");
		            }
		        }
		        System.out.print("  ");
		    }

		    // Print N
		    public static void printN(int row) {
		        for (int j = 0; j < 5; j++) {
		            if (j == 0 || j == 4 || (row == j && row < 3)) {
		                System.out.print("*");
		            } else {
		                System.out.print(" ");
		            }
		        }
		        System.out.print("  ");
		    }

		    // Print I
		    public static void printI(int row) {
		        for (int j = 0; j < 5; j++) {
		            if (row == 0 || row == 4 || j == 2) {
		                System.out.print("*");
		            } else {
		                System.out.print(" ");
		            }
		        }
		        System.out.print("  ");
		    }

		    // Print G
		    public static void printG(int row) {
		        for (int j = 0; j < 5; j++) {
		            if ((row == 0 && j != 4) || (row == 4 && j != 0 && j != 3) || (j == 0 && row > 0 && row < 4) || (j == 3 && row == 2)) {
		                System.out.print("*");
		            } else {
		                System.out.print(" ");
		            }
		        }
		        System.out.print("  ");
		    }
		}


