package Learnarray2;

		public class SwapNumbersWithout {
		    public static void main(String[] args) {
		        int a = 5, b = 99;

		        System.out.println("Before swapping:");
		        System.out.println("a = " + a);
		        System.out.println("b = " + b);

		        // Swapping without third variable
		        a = a + b;
		        b = a - b;
		        a = a - b;

		        System.out.println("\nAfter swapping:");
		        System.out.println("a = " + a);
		        System.out.println("b = " + b);
		    }
		}


	