package Learnarray2;

	public class ArraySwapnet {
	    public static void main(String[] args) {
	        // Sample array
	        int[] array = {1, 2, 3, 4, 5};

	        // Indices of elements to swap
	        int index1 = 1;
	        int index2 = 3;

	        // Display original array
	        System.out.println("Original Array:");
	        displayArray(array);

	        // Swapping elements
	        int temp = array[index1];
	        array[index1] = array[index2];
	        array[index2] = temp;

	        // Display array after swapping
	        System.out.println("\nArray after swapping:");
	        displayArray(array);
	    }

	    // Method to display array elements
	    public static void displayArray(int[] arr) {
	        for (int num : arr) {
	            System.out.print(num + " ");
	        }
	        System.out.println();
	    }
	}



