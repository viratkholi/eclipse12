package Learnarray2;

public class ReverseArray {
    public static void main(String[] args) {
        int[] arr = {2, 7, 4, 5, 6, 3};

        // Printing the original array
        System.out.print("Original Array: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();

        // Reversing the array
        int[] reversedArr = new int[6];
        for (int i = 0; i < arr.length; i++) {
            reversedArr[i] = arr[arr.length - 1 - i];
        }

        // Printing the reversed array
        //System.out.print("Reversed Array: ");
        for (int i = 0; i < reversedArr.length; i++) {
            System.out.print(reversedArr[i] + " ");
        }
    }
}


