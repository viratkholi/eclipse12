package Learnarray2;

public class AverageArray {

	public static void main(String[] args) {
	    int[] arr = {7, 4, 5, 6, 3, 2}; 
	    for (int i = 0; i < arr.length; i++) {
	        for (int j = i + 1; j < arr.length; j++) {
	            if (arr[i] > arr[j]) {
	                int temp = arr[i];
	                arr[i] = arr[j];
	                arr[j] = temp;
	            }
	        }
	        System.out.print(arr[i] + " ");
	    }

	    // Calculate average
	    int sum = 0;
	    for (int i = 0; i < arr.length; i++) {
	        sum += arr[i];   //sum = sum + arr[i];
	    }
	    double average = (double) sum / arr.length;
	    System.out.println("\nAverage: " + average);
	}


}
