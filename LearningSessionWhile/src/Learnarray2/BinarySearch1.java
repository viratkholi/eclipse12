package Learnarray2;

public class BinarySearch1 {

	public static void main(String[] args) {
		int[] ar = { 10, 20, 30, 40, 50 };
		int min = 0;
		int max = ar.length - 1;
		int key = 50;
		while (min <= max) {
			int mid = (min + max) / 2;
			if (key == ar[mid]) {
				System.out.println("present");
				break;

			} else if (key < ar[mid]) {
				max = mid - 1;
			} else {
				min = mid + 1;
			}
		}
		if (min > max) {
			System.out.println("not present");
		}

	}

}
