package Learnarray2;

public class ArrayPracticeTask1net {

	public static void main(String[] args) {
	    int[] arr = {7, 4, 5, 6, 3, 2}; 

	    // Sorting the array using Bubble Sort
	    for (int i = 0; i < arr.length; i++) {
	        for (int j = 0; j < arr.length - 1 - i; j++) {
	            if (arr[j] > arr[j + 1]) {
	                int temp = arr[j];
	                arr[j] = arr[j + 1];
	                arr[j + 1] = temp;
	            }
	        }
	    }

	    // Printing the sorted array
	    System.out.print("Sorted Array: ");
	    for (int k = 0; k < arr.length; k++) {
	        System.out.print(arr[k] + " ");
	    }
	    System.out.println();

	    // Finding and printing the minimum and maximum numbers
	    System.out.println("Minimum number: " + arr[0]);
	    System.out.println("Maximum number: " + arr[arr.length - 1]);

	    // Finding and printing the average
	    int sum = 0;
	    for (int num : arr) {
	        sum += num;
	    }
	    double average = (double) sum / arr.length;
	    System.out.println("Average: " + average);

	    // Finding and printing the second maximum and second minimum
	    System.out.println("Second Minimum: " + arr[1]);
	    System.out.println("Second Maximum: " + arr[arr.length - 2]);
	}


}
