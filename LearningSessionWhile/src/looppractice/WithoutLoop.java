package looppractice;

public class WithoutLoop {

	public static void main(String[] args)
	{
		display(1);
	} 
	public static void display(int res)
	{
		 
	    System.out.println(res);
	    if(res<5)
	    {
	    	res++;
	    	display(res);
	    }
	   
	}
	
	
}
//
//public static void main(String[] args)
//{
//	display(100);
//} 
//public static void display(int res)
//{
//	 
//    System.out.println(res);
//    if(res>1)
//    {
//    	res--;
//    	display(res);
//    }
//   
//}
