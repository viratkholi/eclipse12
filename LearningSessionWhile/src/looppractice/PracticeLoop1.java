package looppractice;

public class PracticeLoop1 {

	public static void main(String[] args) {
		
		PracticeLoop1 st = new PracticeLoop1();
		st.display(1,50);

	}
	public void display(int a, int b)
	{
		while(a<=b) 
		{
			if(a%5!=0)
			{
				System.out.print(a+" ");
			}
			a++;
		}
	}

}
