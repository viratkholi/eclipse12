package PracticeTopics;

public class Typecasting {

	public static void main(String[] args) {
		int a = 10;
		double b = a; // Automatic casting: int to double
		
		System.out.println(a);
		System.out.println(b);

	}

}
