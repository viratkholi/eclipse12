package PracticeTopics;

public class TypeCasting {

	public static void main(String[] args) {
		double a = 55.55;
		int b = (int) a; // Manual casting: double to int
		
		System.out.println(a);
		System.out.println(b);

		int maxScore = 500;
		int userScore = 423;

		float percentage = (float) userScore/maxScore *100f;

		System.out.println("User's percentage is " + percentage);

	}

}
