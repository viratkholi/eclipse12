package practicetest;

public class SumofDigits {

	public static void main(String[] args) {
		int no = 144;
		int total = 0;
		int rem = no%10;
		total = total + rem;
		while(no>0) {
			no = no/10;
			rem = no%10;
			total = total + rem;
		}
		System.out.println(total);

	}

}
//sum of digits