package Arraytypes;
	
	public class StudentMarks1 {
	    public static void main(String[] args) {
	        // Define class names
	        String[] classNames = {"Class A", "Class B", "Class C"};
	        
	        // Define student names
	        String[][] studentNames = {
	        		 {"Thaniga", "Ramu", "Siva"},
	 	             {"Kholi", "Dhoni", "Rohith"},
	 	             {"Ronaldo", "Messi", "Neymer"}
	        };
	        
	        // Define marks for each student
	        int[][][] marks = {
	            { // Class A
	                {70, 80, 90, 85, 75}, // Thaniga's marks
	                {60, 75, 85, 90, 70}, // Ramu's marks
	                {80, 85, 95, 70, 80}  // Siva's marks
	            },
	            { // Class B
	                {65, 75, 80, 70, 85}, // Kholi's marks
	                {55, 65, 75, 80, 90}, // Dhoni's marks
	                {75, 85, 80, 90, 95}  // Rohith's marks
	            },
	            { // Class C
	                {80, 90, 85, 95, 70}, // Ronaldo's marks
	                {70, 80, 75, 85, 80}, // Messi's marks
	                {90, 95, 85, 80, 90}  // Neymer's marks
	            }
	        };
	        
	        // Print marks for each student in each class
	        for (int i = 0; i < classNames.length; i++) {
	            System.out.println(classNames[i] + ":");
	            int classTopper = 0;
	            int classTopperTotal = 0;
	            for (int j = 0; j < studentNames[i].length; j++) {
	                int studentTotal = 0;
	                System.out.println("  " + studentNames[i][j] + ":");
	                for (int k = 0; k < marks[i][j].length; k++) {
	                    System.out.println("    Subject " + (k + 1) + ": " + marks[i][j][k]);
	                    studentTotal += marks[i][j][k];
	                }
	                System.out.println("    Total Marks: " + studentTotal);
	                if (studentTotal > classTopperTotal) {
	                    classTopperTotal = studentTotal;
	                    classTopper = j;
	                }
	            }
	            System.out.println("  Class Topper: " + studentNames[i][classTopper] + " with total marks: " + classTopperTotal);
	            System.out.println();
	        }
	    }
	}

