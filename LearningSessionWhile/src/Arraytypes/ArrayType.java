package Arraytypes;

public class ArrayType {
    public static void main(String[] args) {
        int[][] arr = {
            {78, 67, 90, 68, 79},
            {98, 78, 67, 98, 99},
            {56, 78, 90, 97, 96},
            {98, 78, 77, 99, 90},
            {12, 23, 34, 45, 56}
        };
        		
        
        int maxNumber = arr[0][0];
        
        for (int i = 1; i < arr.length; i++) {
            if (arr[0][i] > maxNumber) {
                maxNumber = arr[0][i];
            }
        }
        
                System.out.println("The maximum number in the first sub-array is: " + maxNumber);
    }
}
