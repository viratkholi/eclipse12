package Practicesession;

import java.util.Scanner;

public class SquareRoot {
    public static int sqrt(int x) {
        if (x == 0 || x == 1)
            return x;

        long start = 1;
        long end = x;
        long ans = 0;

        while (start <= end) {
            long mid = (start + end) / 2;    //1+x/2
            long square = mid * mid;         

            if (square == x)
                return (int)mid;
            else if (square < x) {
                start = mid + 1;
                ans = mid;
            } else {
                end = mid - 1;
            }
        }

        return (int)ans;
    }

    public static void main(String[] args) {
        Scanner sc =new Scanner(System.in);
        System.out.println("Enter the number x1");
        int x1=sc.nextInt();
        System.out.println("Enter the number x2");
        int x2=sc.nextInt();


        System.out.println("Square root of " + x1 + " is " + sqrt(x1));
        System.out.println("Square root of " + x2 + " is " + sqrt(x2));
    }
}
