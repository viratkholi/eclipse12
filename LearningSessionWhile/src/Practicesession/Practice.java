package Practicesession;

public class Practice {

	public static void main(String[] args) {
		String name = "Thaniga Kalanidhi";
		char [] ch = name.toCharArray();
        int count =0;
        for (char c : ch) {
			count++;
		}
        System.out.println(count);
        
        //2nd method
        String name1 = "Siva Kumar";
        int i=0;
        for(;true;) {
        	try {
        	System.out.println(name1.charAt(i));
        	i++;
        	}catch(StringIndexOutOfBoundsException se) {
        		System.out.println(i);
        		break;
        	}
        }
	}

}
