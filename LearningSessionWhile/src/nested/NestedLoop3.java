package nested;

public class NestedLoop3 {

	public static void main(String[] args)
	{
		display();
	}
	public static void display() 
	{
		for(int i=1; i<=10; i++)
		{
			System.out.print(i+" ");
			if(i%5 == 0)
			{
				System.out.println();
			}
		}
	}

}
