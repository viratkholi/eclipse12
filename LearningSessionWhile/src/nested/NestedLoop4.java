package nested;

public class NestedLoop4 {

	public static void main(String[] args) 
	{
		display();

	}
	public static void display()
	{
		for(int row=5; row>=1; row--)
		{
			for(int col=1; col<=row; col++)
			{
			    System.out.print(1+" ");
			}
			System.out.println();
		}
	}

}
