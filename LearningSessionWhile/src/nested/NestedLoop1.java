package nested;

public class NestedLoop1 {

	public static void main(String[] args) 
	{
		display();
	}
	public static void display() 
	{
		for(int i=1; i<5; i++)
		{
			for(int j=0; j<5; j++)
			{
				System.out.print(j+" ");
			}
			System.out.println();
		}
	}

}
