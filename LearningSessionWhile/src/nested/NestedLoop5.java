package nested;

public class NestedLoop5 {

	public static void main(String[] args) {
		display();

	}
	public static void display()
	{
		for(int row=1; row<=5; row++)
		{
			for(int col=1; col<=row; col++)
			{
				System.out.print(row+" ");
			}
			System.out.println();
		}
	}

}
