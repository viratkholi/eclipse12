package LearnArray;

public class ArrayExamplenet {

	    public static void main(String[] args) {
//	        int[] sourceArray = {1, 2, 3, 4, 5};
//	        int[] destinationArray = new int[sourceArray.length];
//
//	        moveElements(sourceArray, destinationArray);
//
//	        System.out.println("Elements moved successfully!");
//	        System.out.println("Destination Array:");
//	        for (int num : destinationArray) {
//	            System.out.print(num + " ");
//	        }
//	    }
//
//	    public static void moveElements(int[] source, int[] destination) {
//	        if (source.length != destination.length) {
//	            throw new IllegalArgumentException("Arrays must have the same length");
//	        }
//
//	        for (int i = 0; i < source.length; i++) {
//	            destination[i] = source[i]; // Copy each element from source to destination
//	        }
	    	int [] ar1 = {10,20,25,35,45,50};
	    	int [] ar2 = new int[ar1.length]; //Create ar2 with the same length
	    	
	    	//copy elements from ar1 to ar2
	    	for(int i=0; i<ar1.length; i++) {
	    		ar2[i] = ar1[i];
	    	}
	    	
	    	//print ar2 to verify the copy
	    	//System.out.println("Elements of ar2 (copied from ar1):");
	    	for(int num : ar2) {
	    		System.out.print(num + " ");
	    	}
	    }
}

