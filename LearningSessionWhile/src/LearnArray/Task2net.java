package LearnArray;

import java.util.HashSet;
import java.util.Set;

public class Task2net {

	public static void main(String[] args) {
	        int[] arr = {10, 15, 20, 10, 11, 23, 20,15};

	        Set<Integer> distinctElements = new HashSet<>();
	        Set<Integer> duplicateElements = new HashSet<>();

	        for (int num : arr) {
	            if (!distinctElements.add(num)) { // If num already exists in distinctElements, it's a duplicate
	                duplicateElements.add(num);
	            }
	        }

	        // Print distinct elements
	        System.out.print("Distinct elements: ");
	        for (int num : arr) {
	            if (!duplicateElements.contains(num)) {
	                System.out.print(num + " ");
	            }
	        }
	        System.out.println();

	        // Print duplicate elements
	        System.out.print("Duplicate elements: ");
	        for (int num : duplicateElements) {
	            System.out.print(num + " ");
	        }
	        System.out.println();
	    
	}

}
