package Typecasting;

public class Cricket extends Student {

	public static void main(String[] args) {
		
		//widen casting(child to parent)
		Cricket cc = new Cricket();
		Student ss = cc;
		ss.study();
		//ss.play();    play method call pana mudiyathu because cricket to student mathiyachu so study matum tha call pana mudium
		
		
		cc.study();
		cc.play();
		
		//narrow casting(parent to child) 
		Cricket c2 = (Cricket)ss;
		     //or
		cc = (Cricket)ss;
		cc.study();
		cc.play();
		
		
	}
	public void play() {
		System.out.println("Playing");
	}
}
