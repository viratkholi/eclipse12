package Typecasting;

public class TypeCastingDemo {

	public static void main(String[] args) {
		//widen casting
		int i = 10;
		float f = i;    //int to float ahh typecaste pandrom
		
		//narrow casting
		int i1 = (int) f;
		System.out.println(f);
	}

}
