package MultiThreading;

public class ThreadDemo {

	public static void main(String[] args) {
		
		ThreadChild gg = new ThreadChild();
		gg.start();
		System.out.println(gg.getId());
		System.out.println(gg.getName());
		System.out.println(gg.getStackTrace());
		System.out.println(gg.getState());
		System.out.println(gg.getPriority());
		System.out.println(gg.isAlive());
		System.out.println(gg.isDaemon());
		System.out.println(gg.isInterrupted());
		
		for(int i=0;i<5; i++)
		    {
			System.out.println("Thread Demo" + i);
			}

	}

	

}
