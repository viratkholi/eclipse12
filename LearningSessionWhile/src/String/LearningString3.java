package String;

public class LearningString3 {

	public static void main(String[] args) {
		//valueof
		//boolean to string
		Boolean b = true;
		String s = String.valueOf(false);
		System.out.println(s);
		
		//char to string
		char [] ch = {'a', 'b', 'c', 'd'};
		String s1 = String.valueOf(ch);
		System.out.println(s1);
		
		//limit and count
		char [] ch1 = {'A', 'B', 'C', 'D', 'E'};
		String s2 = String.valueOf(ch1, 2, 3);
		System.out.println(s2);

	}

}
