package String;

public class LearningString2 {

	public static void main(String[] args) {
		
		//equals
		String name = "Thaniga";
		String name1 = new String("Thaniga");
		Boolean s = name.equals(name1);
		System.out.println(s);
		System.out.println(name == name1);
		
		//equals ignorecase
		String name2 = "Kholi";
		String name3 = "kholi";
		System.out.println(name2.equals(name3));
		Boolean s1 = name2.equalsIgnoreCase(name3);
		System.out.println(s1);
		
		//indexof
		String name4 = "Ajith";
		String name5 = "Thala";
		System.out.println(name5.indexOf("T"));
		
		//indexof repeating letters
		String name6 = "Sivakumar";
		int index = name6.indexOf("a", 4);
		System.out.println(index);
		
		//isempty
		String name7 = " ";
		System.out.println(name7.isEmpty());
		
		//join
		String s2 = String.join("/", "14", "11", "2001");
		System.out.println(s2);
		
		//lastindexof
		String name8 = "Chithra";
		System.out.println(name8.lastIndexOf("h"));
		
		//replaceof
		String name9 = "Subeshni";
		System.out.println(name9.replace("e", "a"));
		
		//startswith
		String name10 = "Grandma";
		System.out.println(name10.startsWith("G"));
		
		//endswith
		String name11 = "Grandpa";
		System.out.println(name11.endsWith("a"));
		
		//subsequence
		String name12 = "Goals";
		System.out.println(name12.subSequence(1, 3));
		
		//substring
		String name13 = "Motivation";
		System.out.println(name13.substring(2));
		
		String name14 = "Kingdom";
		System.out.println(name14.substring(2, 5));
		
		//string to chararray
		String name15 = "Gavaskar";
		char[] s5 = name15.toCharArray();
		for(int i=0; i<s5.length; i++)
		{
			System.out.println(s5[i]);
		} 
	    
		//LowerCase
		String name16 = "Murugar";
		System.out.println(name16.toLowerCase());
		
		//uppercase
		String name17 = "Kovil";
		System.out.println(name17.toUpperCase());
		
	    //Trim
		String name18 = "Siva             ";
		System.out.println(name18.trim());
		
	}

}
