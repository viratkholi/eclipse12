package String;

public class Tasks {

	public static void main(String[] args) {
       //without char method
		String name = "muthuramalingam";
		char[] ch = new char[name.length()];
		{
			for(int i=0;i<name.length();i++)
			{
				ch[i] = name.charAt(i);
			}
		}
		for (char c : ch) {
			System.out.print(c + " ");
		}
		System.out.println();
		
		//practice
		String player = "Virat";
		System.out.print(player.toCharArray());
		
		System.out.println();
		
		for(int j=0; j<player.length();j++) {
		System.out.println(player.charAt(j));
		}
	}

}
