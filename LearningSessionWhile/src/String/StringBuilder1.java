package String;

public class StringBuilder1 {

	public static void main(String[] args) {
		
		//StringBuffer
	     StringBuilder s = new StringBuilder("Siva");
	     System.out.println(s);
	     s.append("kumar");
	     System.out.println(s);       //Diff between string and stringbuffer
	     
//	     //String
//	     String s1 = "Thaniga";
//	     System.out.println(s1);
//	     String s0 = s1.concat("kalandhi");
//	     System.out.println(s0);
	     
	     //delete
	     System.out.println("====" + s);
	     StringBuilder s0 = s.delete(1, 4);
	     System.out.println(s0);
	     
	     //Comapareto
	     StringBuilder s1 = new StringBuilder("Thaniga");
	     StringBuilder s2 = new StringBuilder("thaniga");
	     
	     int no = s1.compareTo(s2);
	     System.out.println(no);
	     
	     //insert
	     StringBuilder s3 = s.insert(1, "King");
	     System.out.println(s3);
	     
	     //replace 
	     StringBuilder s4 = s.replace(1, 5, "Queen");
	     System.out.println(s4);
	     
	     //reverse
	     StringBuilder s5 =s.reverse();
	     System.out.println(s5);
	     
	          //or
	     
	     String name1 = "Appa";
	     StringBuilder sb12 = new StringBuilder(name1);
	     sb12.reverse();
	     System.out.println(sb12);
	     
	     //substring
	     StringBuilder sb13 = new StringBuilder("Kanchipuram");
	     String s6= sb13.substring(1, 5);
	     System.out.println(s6);
	     
	      //or 
	     String s7 = sb13.substring(3);
	     System.out.println(s7);
	     
	     
	     
	     
	     
	}

}
