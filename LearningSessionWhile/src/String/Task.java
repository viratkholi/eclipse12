package String;

public class Task {
		
		    public static void main(String[] args) {
		        String name2 = "iyampettai ariudainambi kaliyaperumal chandran";
		        String reversed = reverseWords(name2);
		        System.out.println(reversed);
		    }

		    public static String reverseWords(String str) {
		        String[] words = str.split("\\s+");
		        StringBuilder reversed = new StringBuilder();
		        for (int i = 0; i < words.length; i++) {
		            if (i > 0) {
		                reversed.append(" ");
		            }
		            String word = words[i];
		            if (i == 1 || i == 3) { // Reverse 2nd and 4th words
		                StringBuilder reversedWord = new StringBuilder();
		                for (int j = word.length() - 1; j >= 0; j--) {
		                    reversedWord.append(word.charAt(j));
		                }
		                reversed.append(reversedWord);
		            } else {
		                reversed.append(word);
		            }
		        }
		        return reversed.toString();
		    }
}

		
