package String;

public class Practice {

	public static void main(String[] args) {
		
		String s = new String("Dhoni");
		String s2 = "Thaniga";
		String s3 = "Rohith";
//		System.out.println(s.hashCode());
//		System.out.println(s2.hashCode());
		System.out.println(System.identityHashCode(s2));
		System.out.println(System.identityHashCode(s));
//		char vat = s.charAt(4);
//		System.out.println(vat);
		
		//printing characters each by each
		for(int i=0; i<s.length(); i++)
		{
			System.out.print(s.charAt(i) + "  "); //printing Characters 
		}
		System.out.println();
		//length
		System.out.println(s.length());  //length of s
		
		//concat method
		String s0 = s2.concat("Sivakumar"); //ThanigaSivakumar
		System.out.println(s0);
		
		String s01 = s3.concat("Sharma"); //RohithSharma
		System.out.println(s01);
		
		s = s.concat("Mahendrasingh"); //DhoniMahendrasingh
		System.out.println(s);
		
		boolean s03 = s2.contains("Tha"); //Thaniga
		System.out.println(s03);
		
		boolean s04 = s3.endsWith("th");  //Rohith
		System.out.println(s04);
		
			
	}

}
