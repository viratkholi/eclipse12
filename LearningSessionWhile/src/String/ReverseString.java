package String;

public class ReverseString {

	public static void main(String[] args) {
	
		 String name = "ThanigaKalanidhi";
		 int start = name.length()-1;         //or 15
		 int end = 0;
		 
		 while(start>=end)
		 {
			 System.out.print(name.charAt(start) + " ");
			 start--;
		 }
	}

}
