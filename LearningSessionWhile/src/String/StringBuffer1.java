package String;

public class StringBuffer1 {

	public static void main(String[] args) {
		
		//StringBuffer
	     StringBuffer s = new StringBuffer("Siva");
	     System.out.println(s);
	     s.append("kumar");
	     System.out.println(s);       //Diff between string and stringbuffer
	     
//	     //String
//	     String s1 = "Thaniga";
//	     System.out.println(s1);
//	     String s0 = s1.concat("kalandhi");
//	     System.out.println(s0);
	     
	     //delete
	     System.out.println("====" + s);
	     StringBuffer s0 = s.delete(1, 4);
	     System.out.println(s0);
	     
	     //Comapareto
	     StringBuffer s1 = new StringBuffer("Thaniga");
	     StringBuffer s2 = new StringBuffer("thaniga");
	     
	     int no = s1.compareTo(s2);
	     System.out.println(no);
	     
	     //insert
	     StringBuffer s3 = s.insert(1, "King");
	     System.out.println(s3);
	     
	     //replace 
	     StringBuffer s4 = s.replace(1, 5, "Queen");
	     System.out.println(s4);
	     
	     //reverse
	     StringBuffer s5 =s.reverse();
	     System.out.println(s5);
	     
	          //or
	     
	     String name1 = "Appa";
	     StringBuffer sb12 = new StringBuffer(name1);
	     sb12.reverse();
	     System.out.println(sb12);
	     
	     //substring
	     StringBuffer sb13 = new StringBuffer("Kanchipuram");
	     String s6= sb13.substring(1, 5);
	     System.out.println(s6);
	     
	      //or 
	     String s7 = sb13.substring(3);
	     System.out.println(s7);
	     
	     
	     
	}

}
