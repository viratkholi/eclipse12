package String;

public class PracticeProg {

	public static void main(String[] args) {
		StringBuffer name = new StringBuffer("Virat");
		int max = 0;
		char letter = ' ';
		  

		for(int j=0;j<name.length();j++)
		{
		  char ch = name.charAt(j);
		  int count = 1; 
		if(ch=='*') {
		  continue;
		}
		  
		  for(int i=j+1; i<name.length();i++)
		  {
		    if(ch == name.charAt(i))
		    {
		      name.setCharAt(i, '*');
		      count++;
		    }
		  }
		if(count>max)
		{
		    max = count;
		    letter = ch; 
		}
		System.out.println(ch + " appears " + count + " times");
		}
		System.out.println(letter + " --> "+ max);
	}

}
