package FileHandling;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class LearnFile {

	public static void main(String[] args) throws IOException {
		
		File gg = new File("/home/thaniga/Public/FileHandling/thaniga.txt");
		gg.createNewFile();
		
		//editing or writing in a file
		FileWriter pencil = new FileWriter(gg);
	    pencil.write("Thaniga"); 
	    pencil.write("Sivakumar");
	    
	    pencil.flush();
	    pencil.close();
	    
	    //append
//	    FileWriter pencil =new FileWriter(gg,true);
//	    pencil.write("1234");
	    
	    //file reader returning int
	    FileReader reader = new FileReader(gg);
//	    int i = reader.read();
//	    char ch = (char)i;
//	    System.out.println(ch);
//	    System.out.println(i);
	    
	    int i = reader.read();
	    while(i != -1)
	    {
	    char ch = (char)i;
	    System.out.print(ch+ " ");
	    i = reader.read();
	    }
	   
	    
	 
		//normal filehandling
		
//		System.out.println(gg.canRead());
//		System.out.println(gg.canWrite());
//		System.out.println(gg.exists());
//		System.out.println(gg.getAbsolutePath());
//		System.out.println(gg.getName());
//		System.out.println(gg.getPath());
//		System.out.println(gg.isDirectory());
//		System.out.println(gg.isFile());
//		System.out.println(gg.isHidden());
		
          //creating new file
		
//		File ff = new File("/home/thaniga/Documents");
//		File[] file = ff.listFiles();
//		for(int i=0; i<file.length; i++)
//		{
//			//System.out.println(file[i]);
//			System.out.println(file[i].getName());
//		}

	}

}
