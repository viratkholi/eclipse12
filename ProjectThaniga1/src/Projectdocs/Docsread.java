package Projectdocs;

import java.io.FileInputStream;
import java.io.IOException;



public class Docsread {

	public static void main(String[] args) {
		try {
            // Specify the path to your .docx file
            String filePath = "/home/thaniga/Documents/Virat.docx";
            
            // Open the .docx file
            FileInputStream fis = new FileInputStream(filePath);
            XWPFDocument docx = new XWPFDocument(fis);
            
            // Get the text from the document
            XWPFWordExtractor extractor = new XWPFWordExtractor(docx);
            String text = extractor.getText();
            
            // Print the extracted text
            System.out.println(text);
            
            // Close the input stream
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
   }
	}

}
