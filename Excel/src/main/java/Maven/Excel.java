package Maven;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Excel {
    
    public static void main(String[] args) {
        try {
            // Specify the path to the Excel file
            String filePath = "/home/thaniga/Downloads/Chennai_batting.xlsx";

            // Create a FileInputStream to read the Excel file
            FileInputStream fis = new FileInputStream(new File(filePath));

            // Create a Workbook instance for the Excel file
            Workbook workbook = WorkbookFactory.create(fis);

            // Get the first sheet in the Excel file
            Sheet sheet = workbook.getSheetAt(0);

            // Find the header row
            Row headerRow = sheet.getRow(0);
            int playerNameIndex = -1;
            int scoreIndex = -1;
            int ballsFacedIndex = -1;
            int foursIndex = -1;
            int sixesIndex = -1;
            int strikeRateIndex = -1;

            // Find the index of each column
            for (Cell cell : headerRow) {
                String header = cell.getStringCellValue().trim(); // Trim to remove any extra spaces
                switch (header) {
                    case "player_Name":
                        playerNameIndex = cell.getColumnIndex();
                        break;
                    case "score":
                        scoreIndex = cell.getColumnIndex();
                        break;
                    case "ball_faced":
                        ballsFacedIndex = cell.getColumnIndex();
                        break;
                    case "no_of_fours":
                        foursIndex = cell.getColumnIndex();
                        break;
                    case "no_of_six":
                        sixesIndex = cell.getColumnIndex();
                        break;
                    case "strike":
                        strikeRateIndex = cell.getColumnIndex();
                        break;
                }
            }
            
            // Print headers
            System.out.println("Player Name\tScore\tBalls Faced\tFours\tSixes\tStrike Rate");

            // Check if all required headers are found
            if (playerNameIndex < 0 || ballsFacedIndex < 0 ||
                foursIndex < 0  ||strikeRateIndex < 0) {
                System.out.println("Required headers not found in the Excel file.");
                return; // Exit the program
            }

            // Iterate through each row in the sheet
            for (Row row : sheet) {
                // Check if the row is the header row
                if (row.getRowNum() == 0) {
                    // Skip the header row
                    continue;
                }

                // Get values from cells dynamically using the column indexes found
                String playerName = row.getCell(playerNameIndex).getStringCellValue();
                double score = row.getCell(scoreIndex).getNumericCellValue();
                double ballsFaced = row.getCell(ballsFacedIndex).getNumericCellValue();
                double fours = row.getCell(foursIndex).getNumericCellValue();
                double sixes = row.getCell(sixesIndex).getNumericCellValue();
                double strikeRate = row.getCell(strikeRateIndex).getNumericCellValue();

                // Check if the score is not zero
                if (score != 0) {
                    // Print player details
                 // Print player details with gaps between columns
                 System.out.println(playerName + "\t\t" + score + "\t" + ballsFaced + "\t" +
                                     fours + "\t" + sixes + "\t" + strikeRate);

                }
            }

            // Find and print player with the highest score
         // Find and print player with the highest score
            findPlayerWithHighestScore(sheet, playerNameIndex, scoreIndex);

            // Close the workbook and the FileInputStream
            workbook.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    // Method to find and print player with the highest score
    private static void findPlayerWithHighestScore(Sheet sheet, int playerNameIndex, int scoreIndex) {
        double maxScore = Double.MIN_VALUE;
        String playerWithMaxScore = "";
        
        for (Row row : sheet) {
            if (row.getRowNum() == 0) {
                continue; // Skip header row
            }

            double score = row.getCell(scoreIndex).getNumericCellValue();
            if (score > maxScore) {
                maxScore = score;
                playerWithMaxScore = row.getCell(playerNameIndex).getStringCellValue();
            }
        }
        
      //  System.out.println("\nPlayer with the highest score:");
        System.out.println("\nPlayer Name: " + playerWithMaxScore);
        System.out.println("Score: " + maxScore);
//        System.out.println("Mustafizur Rahman");

    }
}
