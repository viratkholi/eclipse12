package com.Thaniga.PROS.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.Thaniga.PROS.entity.Entity1;
import com.Thaniga.PROS.repository.Repository1;

@Controller
public class controller {
    
	@Autowired
	Repository1 repo;
	
	@RequestMapping("Class")
	public ModelAndView Student(Entity1 cl) {
		System.out.println("Thaniga");
		ModelAndView mav = new ModelAndView("studetails");
		mav.addObject("bench", cl);
		return mav;
	}
	@PostMapping("addstud")
	public String Studentsave(Entity1 cl) {
		repo.save(cl);
		System.out.println(cl.getStudname());
		return "redirect:/Class";
		
	}
	@RequestMapping("show")
	public ModelAndView Studshow() {
		ModelAndView mav1 = new ModelAndView("studdetailss");
		System.out.println("show");
		List l1 = repo.findAll();
		mav1.addObject("room", l1);
		return mav1;
	}
	 @RequestMapping("update{pen}")
	   public ModelAndView updateEmp(@PathVariable int pen) {
		   Entity1 ent2 = repo.findById(pen).get();
		   ModelAndView mav3 = new ModelAndView("updatestud");
		   mav3.addObject("obj2",ent2);
		   return mav3;
}
	 @RequestMapping("delete{pencil}")
	   public String deleteEmployee(@PathVariable int pencil) {
		   repo.deleteById(pencil);
		   return "redirect:show";
	   }
}
