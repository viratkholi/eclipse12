package com.Thaniga.PROS.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Entity
public class Entity1 {
	
	@Id
	private int studid;
	private String studname;
	private double studheight;
	private char studgender;
	
	public int getStudid() {
		return studid;
	}
	public void setStudid(int studid) {
		this.studid = studid;
	}
	public String getStudname() {
		return studname;
	}
	public void setStudname(String studname) {
		this.studname = studname;
	}
	public double getStudheight() {
		return studheight;
	}
	public void setStudheight(double studheight) {
		this.studheight = studheight;
	}
	public char getStudgender() {
		return studgender;
	}
	public void setStudgender(char studgender) {
		this.studgender = studgender;
	}
	
	
	
}
