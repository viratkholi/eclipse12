package com.Thaniga.MMSs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MmSsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MmSsApplication.class, args);
	}

}
