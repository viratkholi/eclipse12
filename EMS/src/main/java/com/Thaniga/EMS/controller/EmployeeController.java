package com.Thaniga.EMS.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.Thaniga.EMS.Entity.EmployeeEntity;
import com.Thaniga.EMS.repository.EmployeeRepo;

@Controller
public class EmployeeController {
	
	    @Autowired
	    EmployeeRepo repo;
	    
	
	
       @RequestMapping("hope")
       public ModelAndView add()
       {
		   System.out.println("Appa is an Emotion");
		   ModelAndView mav = new ModelAndView("Index");
		   ArrayList list = new ArrayList();
		   list.add("Ramu");
		   list.add("Thaniga");
		   mav.addObject("names", list);
		   return mav;
	   }
	
	
	   @RequestMapping("Thaniga")
       public void display(){
		   
    	   System.out.println("Virat");
       }
	   
	   @RequestMapping("Kholi")
       public String display1(){
		   
    	   System.out.println("Dhoni");
		return "Index";
       }
	   @RequestMapping("Sachin")
       public String display2(){
		   
    	   System.out.println("Tendulkar");
		return "Chess";
       }
	   @RequestMapping("user")
	   public void dbDisplay() {
		   System.out.println("Login");
	   }
	   
	   @RequestMapping("view")
	   public ModelAndView dbDisplay1() {
		   System.out.println("view");
		  ModelAndView mav1 = new ModelAndView("list_details");
		  List ll = repo.findAll();
		  mav1.addObject("data", ll);
		  return mav1;
	   }
	   
	   @RequestMapping("/addEmp")
	   public ModelAndView createEmployee(EmployeeEntity ent) {
		   ModelAndView mav2 = new ModelAndView("createEmploy");
		   mav2.addObject("obj1", ent);
		   return mav2;
	   }
	   
	   @PostMapping("/addEmp/newone")
	   public String addEmployee(EmployeeEntity ent1)
	   {
		   //System.out.println("_+_+_+_" + ent1.getName());
		   repo.save(ent1);
		   return "redirect:/view";
	   }
	   @RequestMapping("/update/{d}")
	   public ModelAndView updateEmp(@PathVariable int d) {
		   EmployeeEntity ent2 = repo.findById(d).get();
		   ModelAndView mav3 = new ModelAndView("updateEmp");
		   mav3.addObject("obj2",ent2);
		   return mav3; 
	   }
	   @RequestMapping("/delete/{d1}")
	   public String deleteEmployee(@PathVariable int d1) {
		   repo.deleteById(d1);
		   return "redirect:/view";
	   }
	   
	   
	   
//	   @RequestMapping("addEmp")
//	   public String createEmp() {
//		   System.out.println("Employee to staff");
//		   return "createEmploy";
//	   }
//	   
//	   @RequestMapping("Overview")
//	   public String createEmp(EmployeeEntity emp1) {
//		   System.out.println("RoyalEnfield");
//		   ModelAndView mav2 = new ModelAndView("Working");
//		   mav2.addObject("obj", emp1);
//		   return "createEmploy";
//	   
//
//}
}
