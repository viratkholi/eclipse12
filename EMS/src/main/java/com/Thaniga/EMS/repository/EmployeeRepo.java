package com.Thaniga.EMS.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.Thaniga.EMS.Entity.EmployeeEntity;

public interface EmployeeRepo extends JpaRepository<EmployeeEntity, Integer>{

}
