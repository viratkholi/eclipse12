	package ProjectRam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExceltoJava {

	public static void main(String[] args) throws IOException {
		 try {
             // Specify the path to your Excel file
             FileInputStream file = new FileInputStream(new File("/home/thaniga/Desktop/project1/PROGRAM WORKS/Chennai batting.xlsx"));

             // Create a workbook instance for XLSX file
             Workbook workbook = WorkbookFactory.create(file);

             // Get the first sheet in the workbook
          XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);

             // Iterate through each row in the sheet
             for (Row row : sheet) {
                 // Iterate through each cell in the row
                 for (Cell cell : row) {
                     // Based on the cell type, read and print the cell value
                     switch (cell.getCellType()) {
                         case STRING:
                             System.out.print(cell.getStringCellValue() + "\t");
                             break;
                         case NUMERIC:
                             System.out.print(cell.getNumericCellValue() + "\t");
                             break;
                         case BOOLEAN:
                             System.out.print(cell.getBooleanCellValue() + "\t");
                             break;
                         case FORMULA:
                             System.out.print(cell.getCellFormula() + "\t");
                             break;
                         case BLANK:
                             System.out.print("");
                             break;
                         default:
                             System.out.print("<UNKNOWN>\t");
                     }
                 }
                 System.out.println(); // Move to the next line after each row
             }

             // Close the workbook and file input stream
             workbook.close();
             file.close();
         } catch (IOException e) {
             e.printStackTrace();
         }
     }
}
