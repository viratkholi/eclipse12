package ProjectRam;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImageMoving {

  public static void main(String[] args) {
    
            Path sourceDir = Paths.get("/home/thaniga/Desktop/project2");  // Source directory path
            Path ooty = Paths.get("/home/thaniga/Desktop/project2/OOTY image");  // Destination directory path
            Path kodai = Paths.get("/home/thaniga/Desktop/project2/KODAI image");
       
     String Ooty = "2024-04-15";  // Target date in "yyyy-MM-dd" format
     String Kodai = "2024-04-16"; 
     
     TransferFiles(Ooty,sourceDir,ooty);
     TransferFiles(Kodai,sourceDir,kodai);   
     System.out.println("\n Specified Date images moved succesfully");   
  }
     
  
  public static void TransferFiles(String targetDate,Path sourceDir,Path destDir) {
       
          try {
              SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
              Date target = sdf.parse(targetDate);

              Files.walkFileTree(sourceDir, new SimpleFileVisitor<Path>() {
                  @Override
                  public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                      if (Files.isRegularFile(file) && isTargetDate(file, target) && (file.toString().toLowerCase().endsWith(".jpg") || file.toString().toLowerCase().endsWith(".jpeg")))
                      {
                          moveFile(file, destDir);
                      }
                      return FileVisitResult.CONTINUE;
                  }
              });
          } catch (Exception e) {
              e.printStackTrace();
          }
      }

      private static boolean isTargetDate(Path file, Date targetDate) throws IOException {
          SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
          Date fileDate = new Date(Files.getLastModifiedTime(file).toMillis());
          String formattedFileDate = sdf.format(fileDate);
          String formattedTargetDate = sdf.format(targetDate);
          return formattedFileDate.equals(formattedTargetDate);
      }

      private static void moveFile(Path sourceFile, Path destDir) throws IOException {
          Path destFile = destDir.resolve(sourceFile.getFileName());
          Files.move(sourceFile, destFile, StandardCopyOption.REPLACE_EXISTING);
          System.out.println("Moved: " + sourceFile.toString() + " to " + destFile.toString());
      }
}
      