package com.Thaniga.MMSs.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller

public class ExcelReading {
    
	@RequestMapping("Cricket")
	ModelAndView mav() throws IOException {
		ModelAndView mav = new ModelAndView("index");
		ArrayList ar = new ArrayList();
		try {
			   File file = new File("/home/thaniga/Downloads/thaniga.xlsx");

			   FileInputStream input = new FileInputStream(file);
			   XSSFWorkbook workbook = new XSSFWorkbook(input);

			   XSSFSheet sheet = workbook.getSheetAt(0);

			   int count = sheet.getLastRowNum() - sheet.getFirstRowNum();

			   for (int i = 0; i <= count; i++) {

			    XSSFRow row = sheet.getRow(i); 

			    for (int j = 0; j < row.getLastCellNum(); j++) {

//			          

			     XSSFCell cell = row.getCell(j);
			     DataFormatter dft = new DataFormatter();

			     String value = dft.formatCellValue(cell);
			     
			     System.out.print(value + " ");
			     ar.add(value);
			     mav.addObject("names", ar);

			    }

			    System.out.println(" ");
			   }

			   workbook.close();

			   input.close();
			  } catch (FileNotFoundException e) {

			   // TODO Auto-generated catch block
			   e.printStackTrace();

			  }
		return mav;
		
	}
}
